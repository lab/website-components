import { resolve } from "path";
import { defineConfig } from "vite";
import { globSync } from "glob";

const SRC = resolve(__dirname, "src");
const ENTRYPOINTS = globSync("**/*.html", { cwd: SRC });

export default defineConfig({
  root: SRC,
  base: "./",
  build: {
    outDir: "../dist",
    emptyOutDir: true,
    rollupOptions: {
      input: Object.fromEntries(ENTRYPOINTS.map((e) => [e, resolve(SRC, e)])),
    },
  },
  css: {
    preprocessorOptions: {
      scss: {
        includePaths: [resolve(__dirname, "node_modules")],
      },
    },
  },
});
