window.addEventListener("load", function () {
  const NAVBAR_BUTTON = document.querySelector("button.side-menu");

  // HACK: Work around window resizing whilst the modal is open animating its size,
  // by closing the modal whenever the window is resized
  window.addEventListener("resize", function () {
    NAVBAR_BUTTON.blur();
  });
});
