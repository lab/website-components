const collapse = document.getElementById("collapse");

document.addEventListener("DOMContentLoaded", function () {
  // Force checkboxes to their initial value
  for (const checkbox of this.document.querySelectorAll(
    "input[type=checkbox",
  )) {
    checkbox.checked = checkbox.defaultChecked;
  }
});
