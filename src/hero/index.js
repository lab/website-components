import _ from "underscore";

const SCROLL_INDICATOR = document.getElementById("scroll-indicator");
const CONTENT = document.querySelector(".content-wrapper");

function handleScrollIndicator() {
  // How far down the page does the content start?
  const initialScroll = CONTENT.getBoundingClientRect().top + window.scrollY;

  const contentHeight = CONTENT.getBoundingClientRect().height;

  // How far down the page do we consider the content "read"?
  const scrollTarget = window.innerHeight * 0.75;

  const scrolled =
    (window.scrollY - initialScroll + scrollTarget) / contentHeight;

  const scrolledPercentage = clamp(scrolled * 100, 0, 100);

  SCROLL_INDICATOR.style.width = `${scrolledPercentage.toFixed(2)}%`;
}

function clamp(n, min, max) {
  return Math.min(Math.max(n, min), max);
}

function handleDetailsStick(hero) {
  const details = hero.querySelector("#details");
  const heroIsVisible =
    hero.getBoundingClientRect().bottom >=
    details.getBoundingClientRect().height;
  if (heroIsVisible) {
    details.classList.remove("stuck");
  } else {
    details.classList.add("stuck");
  }
}

function handleIntersection(entries) {
  handleDetailsStick(entries[0].target);
}

window.addEventListener("load", function () {
  const hero = document.getElementById("hero");

  let observer = new IntersectionObserver(handleIntersection, {
    root: null,
    threshold: _.range(0, 1, 0.01), // 1%
  });
  observer.observe(hero);

  handleDetailsStick(hero);

  if (SCROLL_INDICATOR) {
    window.addEventListener("resize", handleScrollIndicator);
    window.addEventListener("scroll", handleScrollIndicator);

    // Initialize the indicator
    handleScrollIndicator();
  }
});
