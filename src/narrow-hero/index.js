import { decode } from "blurhash";

const SCROLL_INDICATOR = document.getElementById("scroll-indicator");
const CONTENT = document.querySelector(".content-wrapper");
const HERO_WRAPPER = document.getElementById("banner-wrapper");

function getScrollPercentage() {
  const contentRect = CONTENT.getBoundingClientRect();

  // How far down the page does the content start?
  const initialScroll = contentRect.top + window.scrollY;

  const contentHeight = contentRect.height;

  // How far down the page do we consider the content "read"?
  const scrollTarget = window.innerHeight * 0.75;

  const scrolled =
    (window.scrollY - initialScroll + scrollTarget) / contentHeight;

  return clamp(scrolled * 100, 0, 100);
}

function handleScrollIndicator() {
  const scrollPercentage = getScrollPercentage();
  SCROLL_INDICATOR.style.width = `${scrollPercentage.toFixed(2)}%`;

  const showIndicator =
    CONTENT.getBoundingClientRect().top <
    SCROLL_INDICATOR.getBoundingClientRect().top;

  SCROLL_INDICATOR.style.opacity = showIndicator ? 1 : 0;
}

function clamp(n, min, max) {
  return Math.min(Math.max(n, min), max);
}

function setHeroBackground() {
  const pixels = decode("LIIyVQwg2nXmQ6j@x[Sv2pS1}kxH", 32, 32);

  const canvas = document.createElement("canvas");
  canvas.width = 32;
  canvas.height = 32;
  const ctx = canvas.getContext("2d");
  const imageData = ctx.createImageData(32, 32);
  imageData.data.set(pixels);
  ctx.putImageData(imageData, 0, 0);

  HERO_WRAPPER.style.backgroundImage = `url(${canvas.toDataURL()})`;
}

window.addEventListener("load", function () {
  if (SCROLL_INDICATOR) {
    window.addEventListener("resize", handleScrollIndicator);
    window.addEventListener("scroll", handleScrollIndicator);

    // Initialize the indicator, matching timeout to transition time
    setTimeout(handleScrollIndicator, 200);
  }

  setHeroBackground();
});
